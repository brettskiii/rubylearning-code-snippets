function multiplier(factor) {
  return function(otherFactor) {
    return factor*otherFactor;
  }
}
