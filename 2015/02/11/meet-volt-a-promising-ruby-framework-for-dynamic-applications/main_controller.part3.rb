def send_message
  unless page._new_message.strip.empty?
    _messages << { sender_id: Volt.user._id, receiver_id: params._user_id, text: page._new_message }
    page._new_message = ''
  end
end

def current_conversation
  _messages.find({ "$or" => [{ sender_id: Volt.user._id, receiver_id: params._user_id }, { sender_id: params._user_id, receiver_id: Volt.user._id }] })
end
